package com.currency.app.ui

import android.content.Context
import com.currency.app.ui.exchange_rate_list.ExchangeRateListPresenter
import com.currency.app.ui.exchange_rate_list.ExchangeRateListView
import com.currency.domain.models.ExchangeRate
import com.currency.domain.repositories.ExchangeRateRepository
import com.currency.domain.usecases.GetExchangeRateListUseCase
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test

class ExchangeRateListPresenterTest {

    private lateinit var testSubject: ExchangeRateListPresenter

    private val view: ExchangeRateListView = mock()

    private val repository: ExchangeRateRepository = mock()

    private val exchangeRateListUseCaseGet: GetExchangeRateListUseCase = GetExchangeRateListUseCase(repository)

    private val testScheduler = TestScheduler()

    private val exchangeRateList = arrayListOf(ExchangeRate("USD", "some text"))

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler({ _ -> testScheduler })
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }
        val context : Context = mock()

        testSubject = ExchangeRateListPresenter(exchangeRateListUseCaseGet, context)
        testSubject.start(view)
    }

    @Test
    fun loadExchangeRateList_givenLoadExchangeRateListSuccess_callsViewOnLoadExchangeRateListSuccess() {
        whenever(repository.getAllExchangeRateList(false)).thenReturn(Single.just(exchangeRateList))

        testSubject.loadExchangeRateList()
        testScheduler.triggerActions()

        verify(view).onLoadExchangeRateListSuccess(exchangeRateList, false)
    }

    @Test
    fun loadExchangeRateList_givenLoadExchangeRateListSuccess_andViewNotAttached_doesNotCallViewOnLoadExchangeRateListSuccess() {
        whenever(repository.getAllExchangeRateList(false)).thenReturn(Single.just(exchangeRateList))
        testSubject.stop()

        testSubject.loadExchangeRateList()
        testScheduler.triggerActions()

        verify(view, never()).onLoadExchangeRateListSuccess(exchangeRateList, false)
    }

    @Test
    fun loadExchangeRateList_givenLoadExchangeRateListError_callsViewOnLoadExchangeRateListError() {
        val throwable = RuntimeException()
        whenever(repository.getAllExchangeRateList(false)).thenReturn(Single.error(throwable))

        testSubject.loadExchangeRateList()
        testScheduler.triggerActions()

        verify(view).onLoadExchangeRateListError(throwable)
    }

    @Test
    fun loadExchangeRateList_givenLoadExchangeRateListError_andViewNotAttached_doesNotCallViewOnLoadExchangeRateListError() {
        val throwable = RuntimeException()
        whenever(repository.getAllExchangeRateList(false)).thenReturn(Single.error(throwable))
        testSubject.stop()

        testSubject.loadExchangeRateList()
        testScheduler.triggerActions()

        verify(view, never()).onLoadExchangeRateListError(throwable)
    }
}
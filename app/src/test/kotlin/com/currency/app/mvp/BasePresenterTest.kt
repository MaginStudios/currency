package com.currency.app.mvp

import com.currency.app.ui.exchange_rate_list.ExchangeRateListView
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test

class BasePresenterTest {

    private lateinit var testSubject: BasePresenterUnderTest

    @Before
    fun setUp() {
        testSubject = spy(BasePresenterUnderTest())
    }

    @Test
    fun start_setsView() {
        val view = ExchangeRateListView()

        testSubject.start(view)

        verify(testSubject).start(view)
    }
}

class BasePresenterUnderTest : BasePresenter<ExchangeRateListView>()
package com.currency.app

import android.app.Application
import com.currency.app.di.AppComponent
import com.currency.app.di.DaggerAppComponent
import timber.log.Timber

class CurrencyApp : Application() {
    companion object {
        lateinit var component: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
            .application(this)
            .build()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
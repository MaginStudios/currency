package com.currency.app.di

import android.app.Application
import android.content.Context
import com.currency.domain.repositories.local.ExchangeRateLocalRepository
import com.currency.domain.repositories.remote.ExchangeRateRemoteRepository
import com.currency.domain.repositories.ExchangeRateRepository
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun exposeExchangeRateRepository(): ExchangeRateRepository

    fun exposeExchangeRateLocalRepository(): ExchangeRateLocalRepository

    fun exposeExchangeRateRemoteRepository(): ExchangeRateRemoteRepository

    fun exposeContext(): Context
}

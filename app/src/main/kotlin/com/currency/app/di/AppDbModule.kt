package com.currency.app.di

import android.content.Context
import com.currency.data.db.createExchangeRateDao
import com.currency.data.service.createExchangeRateService
import com.currency.data.repositories.*
import com.currency.data.repositories.local.ExchangeRateLocalDaoImpl
import com.currency.data.repositories.local.ExchangeRateLocalModelMapperImpl
import com.currency.data.repositories.local.ExchangeRateLocalRepositoryImpl
import com.currency.data.repositories.remote.*
import com.currency.domain.repositories.local.ExchangeRateLocalRepository
import com.currency.domain.repositories.remote.ExchangeRateRemoteRepository
import com.currency.domain.repositories.ExchangeRateRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppDbModule {

    @Singleton
    @Provides
    fun provideExchangeRateDao(context: Context) = createExchangeRateDao(context)

    @Singleton
    @Provides
    fun provideExchangeRateService() = createExchangeRateService()

    @Singleton
    @Provides
    fun provideExchangeRateLocalModelMapper() =
        ExchangeRateLocalModelMapperImpl()

    @Singleton
    @Provides
    fun provideCurrencyRemoteModelMapper() =
        CurrencyRemoteModelMapperImpl()

    @Singleton
    @Provides
    fun provideConversionRateRemoteModelMapper() =
        ConversionRateRemoteModelMapperImpl()

    @Singleton
    @Provides
    fun provideExchangeRateRemoteModelMerger() =
        ExchangeRateRemoteModelMergerImpl()

    @Singleton
    @Provides
    fun provideExchangeRateLocalRepository(
        exchangeRateDbLocalDao: ExchangeRateLocalDaoImpl,
        mapper: ExchangeRateLocalModelMapperImpl
    ): ExchangeRateLocalRepository =
        ExchangeRateLocalRepositoryImpl(
            exchangeRateDbLocalDao,
            mapper
        )

    @Singleton
    @Provides
    fun provideExchangeRateRemoteRepository(
        exchangeRateRemoteService: ExchangeRateRemoteServiceImpl,
        currencyMapper: CurrencyRemoteModelMapperImpl,
        conversionRateMapper: ConversionRateRemoteModelMapperImpl
    ): ExchangeRateRemoteRepository =
        ExchangeRateRemoteRepositoryImpl(exchangeRateRemoteService, currencyMapper, conversionRateMapper)

    @Singleton
    @Provides
    fun provideExchangeRateRepository(
        localRepository: ExchangeRateLocalRepository,
        remoteRepository: ExchangeRateRemoteRepository,
        merger: ExchangeRateRemoteModelMergerImpl
    ): ExchangeRateRepository =
        ExchangeRateRepositoryImpl(localRepository, remoteRepository, merger)
}
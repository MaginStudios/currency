package com.currency.app.ui.exchange_rate_list

import android.content.Context
import com.currency.app.di.PerScreen
import com.currency.domain.repositories.ExchangeRateRepository
import com.currency.domain.usecases.GetExchangeRateListUseCase
import dagger.Module
import dagger.Provides

@Module
class ExchangeRateListModule {

    @PerScreen
    @Provides
    fun provideExchangeRateListUseCase(exchangeRateRepository: ExchangeRateRepository) =
        GetExchangeRateListUseCase(exchangeRateRepository)

    @PerScreen
    @Provides
    fun providePresenter(exchangeRateListUseCaseGet: GetExchangeRateListUseCase, context: Context) =
        ExchangeRateListPresenter(exchangeRateListUseCaseGet, context)
}
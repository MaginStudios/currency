package com.currency.app.ui.exchange_rate_list

import android.widget.TextView
import com.currency.domain.models.ExchangeRate

class ExchangeRateListContract {

    interface View {
        fun onLoadExchangeRateListSuccess(exchangeRateList: List<ExchangeRate>, updated: Boolean)
        fun onLoadExchangeRateListError(throwable: Throwable)

        fun onSelectedCurrencyClicked()
        fun onUserValueClicked()
        fun onDecimalClicked()
        fun onNumberClicked(textView: TextView)
        fun onBackspaceClicked()
        fun onCloseKeyboard()
    }

    interface Presenter {
        fun loadExchangeRateList()
    }
}
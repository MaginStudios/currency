package com.currency.app.ui.exchange_rate_list

import com.currency.app.di.AppComponent
import com.currency.app.di.PerScreen
import dagger.Component

@PerScreen
@Component(modules = arrayOf(ExchangeRateListModule::class),
    dependencies = arrayOf(AppComponent::class))
interface ExchangeRateListComponent {
    fun inject(view: ExchangeRateListView)
}
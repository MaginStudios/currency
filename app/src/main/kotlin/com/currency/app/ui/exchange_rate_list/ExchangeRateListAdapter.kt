package com.currency.app.ui.exchange_rate_list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.currency.app.R
import com.currency.domain.models.ExchangeRate
import kotlinx.android.synthetic.main.exchange_rate_list_item.view.*
import java.util.*

class ExchangeRateListAdapter(val onItemClick: (ExchangeRate) -> Unit,
                              val onItemFavoriteClick: (ExchangeRate, Int) -> Unit) : RecyclerView.Adapter<ExchangeRateListAdapter.ViewHolder>() {

    private val exchangeRateList = mutableListOf<ExchangeRate>()
    private var filteredList : MutableList<ExchangeRate> = mutableListOf()
    private var currencyFactor : Double = 1.0
    private var favorites : MutableSet<String> = mutableSetOf()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        filteredList[position].apply {
            holder.codeTextView.text = code
            holder.valueTextView.text = String.format("%,.2f", value * currencyFactor)
            holder.descriptionTextView.text = description
            if (favorites.contains(code)) {
                holder.favoriteImageView.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.favoriteStarColor))
                holder.favoriteImageView.setImageResource(R.drawable.ic_star)
            } else {
                holder.favoriteImageView.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.gray))
                holder.favoriteImageView.setImageResource(R.drawable.ic_star_outline)
            }
        }
    }

    override fun getItemCount() = filteredList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val container = LayoutInflater.from(parent.context)
            .inflate(R.layout.exchange_rate_list_item, parent, false) as ViewGroup
        val viewHolder = ViewHolder(container)
        container.setOnClickListener { onItemClick(filteredList[viewHolder.adapterPosition]) }
        container.exchange_rate_favorite.setOnClickListener { onItemFavoriteClick(filteredList[viewHolder.adapterPosition], viewHolder.adapterPosition) }
        return viewHolder
    }

    fun initList(exchangeRateList: List<ExchangeRate>, favorites: Set<String>, shouldFilter: Boolean, currencyFactor: Double) {
        this.currencyFactor = currencyFactor
        this.exchangeRateList.clear()
        this.exchangeRateList.addAll(exchangeRateList)
        this.favorites = favorites.toMutableSet()
        if (shouldFilter) {
            this.filteredList = exchangeRateList.filter { favorites.contains(it.code) }.toMutableList()
        } else {
            this.filteredList = exchangeRateList.toMutableList()
        }
        notifyDataSetChanged()
    }

    fun updateCurrencyFactor(currencyFactor: Double) {
        this.currencyFactor = currencyFactor
        notifyDataSetChanged()
    }

    fun filterFavorite(exchangeRate: ExchangeRate, index: Int) {
        favorites.remove(exchangeRate.code)
        filteredList.remove(exchangeRate)
        notifyItemRemoved(index)
    }

    fun removeFavorite(exchangeRate: ExchangeRate, index: Int) {
        favorites.remove(exchangeRate.code)
        notifyItemChanged(index)
    }

    fun addFavorite(code: String, index: Int) {
        favorites.add(code)
        notifyItemChanged(index)
    }
    fun updateFavorites(codes: Set<String>, searchText: String) {
        favorites.clear()
        favorites.addAll(codes)
        filteredList.clear()
        filteredList.addAll(exchangeRateList.filter {
            codes.contains(it.code) && (searchText.isEmpty() || match(it, searchText))
        })
        notifyDataSetChanged()
    }

    fun unShowFavorites(searchText: String) {
        filteredList.clear()
        if (searchText.isEmpty()) {
            filteredList.addAll(exchangeRateList)
        } else {
            filteredList.addAll(exchangeRateList.filter { match(it, searchText) })
        }
        notifyDataSetChanged()
    }

    private fun match(exchangeRate: ExchangeRate, searchText: String): Boolean {
        val text = searchText.toUpperCase(Locale.US)
        return exchangeRate.code.contains(text) || exchangeRate.description.toUpperCase(Locale.US).contains(text)
    }

    fun applySearch(searchText: String, showFavorites: Boolean) {
        if (searchText.isEmpty()) {
            filteredList = exchangeRateList
        }
        filteredList = exchangeRateList.filter {
            match(it, searchText) && (!showFavorites || favorites.contains(it.code))
        } as MutableList<ExchangeRate>
        notifyDataSetChanged()
    }

    class ViewHolder(container: ViewGroup) : RecyclerView.ViewHolder(container) {
        val codeTextView: TextView = container.exchange_rate_code
        val valueTextView: TextView = container.exchange_rate_value
        val descriptionTextView: TextView = container.exchange_rate_description
        val favoriteImageView: ImageView = container.exchange_rate_favorite
    }
}
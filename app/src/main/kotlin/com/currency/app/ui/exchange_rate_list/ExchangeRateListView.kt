package com.currency.app.ui.exchange_rate_list

import android.app.Activity
import android.content.Context
import android.view.*
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.OnClick
import com.currency.app.CurrencyApp
import com.currency.app.R
import com.currency.app.mvp.BaseView
import com.currency.app.mvp.MvpPresenter
import com.currency.app.ui.common.initRecyclerView
import com.currency.app.utils.PrefsConst
import com.currency.app.utils.StringUtils
import com.currency.app.utils.TimeUtils
import com.currency.domain.models.ExchangeRate
import timber.log.Timber
import java.text.DecimalFormat
import javax.inject.Inject

class ExchangeRateListView : BaseView(), ExchangeRateListContract.View {

    @Inject
    lateinit var presenter: ExchangeRateListPresenter

    @BindView(R.id.recycler_view)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.user_value)
    lateinit var userValueEditText: TextView

    @BindView(R.id.currency_code)
    lateinit var currencyCodeText: TextView

    @BindView(R.id.keyboard_view)
    lateinit var keyboardView: ViewGroup

    @BindView(R.id.empty_state_text)
    lateinit var emptyStateText: TextView

    @BindView(R.id.last_update_text)
    lateinit var lastUpdateText: TextView

    private lateinit var searchViewItem: MenuItem

    private val clickListener: (ExchangeRate) -> Unit = this::onExchangeRateClicked
    private val favoriteClickListener: (ExchangeRate, Int) -> Unit = this::onExchangeRateFavoriteClicked

    private val recyclerViewAdapter = ExchangeRateListAdapter(clickListener, favoriteClickListener)

    private var currencyCode: String? = "USD"
    private var currencyValue: Double = 1.0
    private var userValue: Double = 1.0
    private var prevUserInput : String = "1.0"
    private var showFavorites : Boolean = false
    private var favorites = HashSet<String>()
    private var searchText = ""
    private var lastUpdate = 0L

    override fun injectDependencies() {
        DaggerExchangeRateListComponent.builder()
            .appComponent(CurrencyApp.component)
            .exchangeRateListModule(ExchangeRateListModule())
            .build()
            .inject(this)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        view.context.getSharedPreferences(PrefsConst.FILE_NAME, Context.MODE_PRIVATE).also {
            currencyCode = it.getString(PrefsConst.CURRENCY_CODE, "USD")
            currencyValue = it.getFloat(PrefsConst.CURRENCY_VALUE, 1.0f).toDouble()
            userValue = it.getFloat(PrefsConst.USER_VALUE, 1.0f).toDouble()
            showFavorites = it.getBoolean(PrefsConst.SHOW_FAVORITES, false)
            lastUpdate = it.getLong(PrefsConst.LAST_UPDATE, 0L)
            val set = it.getStringSet(PrefsConst.FAVORITES, setOf("USD", "EUR", "JPY"))
            favorites = HashSet(set)
        }
        recyclerView.initRecyclerView(LinearLayoutManager(view.context), recyclerViewAdapter)
        with(presenter) {
            start(this@ExchangeRateListView)
            loadExchangeRateList()
            currencyCodeText.text = currencyCode
            userValueEditText.text = String.format("%,.2f", userValue)
            lastUpdateText.text = buildLastUpdateText()
            emptyStateText.text = resources!!.getString(R.string.empty_state_loading)
        }
    }

    private fun buildLastUpdateText(): String =
        resources!!.getString(R.string.last_update, TimeUtils.format(lastUpdate))

    override fun onLoadExchangeRateListSuccess(exchangeRateList: List<ExchangeRate>, updated: Boolean) {
        if (updated) {
            lastUpdate = TimeUtils.now()
            applicationContext!!.getSharedPreferences(PrefsConst.FILE_NAME, Context.MODE_PRIVATE)
                .edit().putLong(PrefsConst.LAST_UPDATE, lastUpdate).apply()
            lastUpdateText.text = buildLastUpdateText()
            showMessage(R.string.exchange_rates_updated)
        }
        recyclerViewAdapter.initList(exchangeRateList, favorites, showFavorites,getUserValue() / currencyValue)
        emptyStateText.visibility = View.GONE
    }

    private fun getUserValue(): Double {
        var value = userValueEditText.text.toString()
        if (value.isEmpty()) {
            value = prevUserInput
        }
        if (StringUtils.decimalSeparator() == '.') {
            return value.replace(",","").toDouble()
        }
        return value.replace(".", "").replace(",",".").toDouble()
    }

    override fun onLoadExchangeRateListError(throwable: Throwable) {
        Timber.e(throwable)
        emptyStateText.text = resources!!.getString(R.string.empty_state_error)
        showMessage(R.string.exchange_rate_load_error)
    }

    @OnClick(R.id.user_value)
    override fun onUserValueClicked() {
        prevUserInput = userValueEditText.text.toString()
        userValueEditText.text = ""
        keyboardView.visibility = if (keyboardView.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    @OnClick(R.id.decimal_button_text)
    override fun onDecimalClicked() {
        if (!userValueEditText.text.contains(StringUtils.decimalSeparator())) {
            userValueEditText.text = userValueEditText.text.toString().plus(StringUtils.decimalSeparator())
            updateExchangeRates()
        }
    }

    @OnClick(R.id.btn_del)
    override fun onBackspaceClicked() {
        if (userValueEditText.text.isNotEmpty()) {
            userValueEditText.text =
                userValueEditText.text.subSequence(0, userValueEditText.text.length - 1)
            updateExchangeRates()
        }
    }

    @OnClick(R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9)
    override fun onNumberClicked(textView: TextView) {
        userValueEditText.text = userValueEditText.text.toString().plus(textView.text.toString())
        updateExchangeRates()
    }

    @OnClick(R.id.close_keyboard)
    override fun onCloseKeyboard() {
        if (userValueEditText.text.isEmpty()) {
            userValueEditText.text = prevUserInput
            updateExchangeRates()
        }
        keyboardView.visibility = View.GONE
    }

    @OnClick(R.id.currency_code)
    override fun onSelectedCurrencyClicked() {
        showMessage(R.string.select_currency_from_list)
    }

    private fun updateExchangeRates() {
        if (userValueEditText.text.isNotEmpty() && userValueEditText.text != StringUtils.decimalSeparator().toString()) {
            val value = userValueEditText.text.toString()
            val decimalSeparator = StringUtils.decimalSeparator().toString()
            if (value.takeLast(1) != decimalSeparator && value.takeLast(2).first().toString() != decimalSeparator) {
                userValueEditText.text = DecimalFormat("###,###.##").format(getUserValue())
            }
            recyclerViewAdapter.updateCurrencyFactor(getUserValue() / currencyValue)
        }
    }


    private fun onExchangeRateClicked(exchangeRate: ExchangeRate) {
        showMessage(R.string.currency_selected)
        currencyCodeText.text = exchangeRate.code
        currencyCode = exchangeRate.code
        currencyValue = exchangeRate.value
        updateExchangeRates()
        closeSearch()
    }

    private fun onExchangeRateFavoriteClicked(exchangeRate: ExchangeRate, index: Int) {
        if (favorites.contains(exchangeRate.code)) {
            favorites.remove(exchangeRate.code)
            if (showFavorites) {
                recyclerViewAdapter.filterFavorite(exchangeRate, index)
            } else {
                recyclerViewAdapter.removeFavorite(exchangeRate, index)
            }
        } else {
            favorites.add(exchangeRate.code)
            recyclerViewAdapter.addFavorite(exchangeRate.code, index)
        }
    }

    private fun toggleFavorites(item: MenuItem) {
        showFavorites = !showFavorites
        if (showFavorites) {
            item.setIcon(R.drawable.ic_star)
            recyclerViewAdapter.updateFavorites(favorites, searchText)
        } else {
            item.setIcon(R.drawable.ic_star_outline)
            recyclerViewAdapter.unShowFavorites(searchText)
        }
    }

    override fun handleBack(): Boolean {
        if (keyboardView.visibility == View.VISIBLE) {
            onCloseKeyboard()
            return true
        }
        return false
    }

    override fun onActivityPaused(activity: Activity) {
        super.onActivityPaused(activity)
        closeSearch()
        activity.getSharedPreferences(PrefsConst.FILE_NAME, Context.MODE_PRIVATE).edit()
            .also {
                it.putString(PrefsConst.CURRENCY_CODE, currencyCode)
                it.putFloat(PrefsConst.CURRENCY_VALUE, currencyValue.toFloat())
                it.putFloat(PrefsConst.USER_VALUE, getUserValue().toFloat())
                it.putBoolean(PrefsConst.SHOW_FAVORITES, showFavorites)
                it.putStringSet(PrefsConst.FAVORITES, favorites)
            }.apply()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_favorites -> toggleFavorites(item)
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        if (showFavorites) {
            menu.findItem(R.id.action_favorites).setIcon(R.drawable.ic_star)
        }
        searchViewItem = menu.findItem(R.id.action_search)
        val searchView = searchViewItem.actionView as SearchView
        searchView.queryHint = resources?.getString(R.string.search_currency)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                searchText = s
                recyclerViewAdapter.applySearch(s, showFavorites)
                return false
            }
        })
        searchView.setOnCloseListener {
            searchText = ""
            recyclerViewAdapter.applySearch("", showFavorites)
            true
        }
    }

    private fun closeSearch() {
        searchViewItem.collapseActionView()
    }

    override fun getLayoutId() = R.layout.exchange_rate_list_view

    override fun getToolbarTitleId() = R.string.screen_title_exchange_rate

    override fun getMenuId(): Int = R.menu.exchange_rate_list_menu

    override fun getPresenter(): MvpPresenter = presenter
}
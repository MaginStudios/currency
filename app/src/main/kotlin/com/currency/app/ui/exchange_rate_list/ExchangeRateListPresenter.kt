package com.currency.app.ui.exchange_rate_list

import android.content.Context
import com.currency.app.mvp.BasePresenter
import com.currency.app.utils.PrefsConst
import com.currency.app.utils.TimeUtils
import com.currency.domain.usecases.GetExchangeRateListUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class ExchangeRateListPresenter @Inject constructor(
    private val exchangeRateListUseCaseGet: GetExchangeRateListUseCase,
    private val context: Context
) :
    BasePresenter<ExchangeRateListView>(), ExchangeRateListContract.Presenter {

    // TODO: Decouple this for testing
    private fun shouldUpdateDb(context: Context): Boolean {
        val sharedPref = context.getSharedPreferences(PrefsConst.FILE_NAME, Context.MODE_PRIVATE)
        if (sharedPref != null) {
            val lastTime = sharedPref.getLong(PrefsConst.LAST_UPDATE, -1L)
            return lastTime == -1L || TimeUtils.diffInMinutes(TimeUtils.now(), lastTime) > 30
        }
        return false
    }

    override fun loadExchangeRateList() {
        val update = shouldUpdateDb(context)
        exchangeRateListUseCaseGet.setShouldUpdateDb(update)
        disposables.add(
            exchangeRateListUseCaseGet.load()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext {
                    if (exchangeRateListUseCaseGet.shouldUpdateDb()) {
                        Timber.i(">> Remote fetch error -> Local fetch")
                        exchangeRateListUseCaseGet.setShouldUpdateDb(false)
                        exchangeRateListUseCaseGet.load()
                    } else {
                        throw it
                    }
                }
                .subscribe(
                    { view?.onLoadExchangeRateListSuccess(it, exchangeRateListUseCaseGet.shouldUpdateDb()) },
                    { view?.onLoadExchangeRateListError(it) })
        )
    }
}
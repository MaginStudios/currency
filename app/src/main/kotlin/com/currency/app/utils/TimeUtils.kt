package com.currency.app.utils

import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs

object TimeUtils {

    fun now(): Long = Date().time

    fun diffInMinutes(time1: Long, time2: Long): Int {
        val diff = abs(time1 - time2)
        return (diff / 1000 / 60).toInt()
    }

    fun format(time: Long): String {
        if (time == 0L) {
            return "-"
        }
        return SimpleDateFormat("HH:mm · dd/MM/yy", Locale.US).format(Date(time))
    }

}
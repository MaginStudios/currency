package com.currency.app.utils

object PrefsConst {

    const val FILE_NAME = "currencyApp"
    const val CURRENCY_CODE = "currencyCode"
    const val CURRENCY_VALUE = "currencyValue"
    const val USER_VALUE = "userValue"
    const val SHOW_FAVORITES = "showFavorites"
    const val FAVORITES = "favorites"
    const val LAST_UPDATE = "lastUpdate"

}
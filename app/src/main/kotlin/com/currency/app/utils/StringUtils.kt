package com.currency.app.utils

import java.text.DecimalFormatSymbols

object StringUtils {

    fun decimalSeparator(): Char = DecimalFormatSymbols.getInstance().decimalSeparator

}
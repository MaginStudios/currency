package com.currency.app.mvp

interface MvpPresenter {
    fun stop()
    fun destroy()
}
<p align="center">
 <img src ="https://gitlab.com/MaginStudios/currency/raw/master/currency_app.png", height=350/>
</p>

# Currency · Android App
This is a currency converter application. It includes the following operations:
- Select a currency
- Enter an amount for the selected currency
- See exchange rates for the selected currency
- Save favorite currencies
- Search currencies

Technical details:
- **Kotlin** · Language
- **Clean Architecture** · Architecture
- **Androidx** · Android Extension Library
- **RxJava** · Reactive Programming
- **Dagger 2** · Dependency Injection
- **Room** · Persistence
- **Conductor** · Navigation
- **Butterknife** · View Injection
- **Timber** · Logging
- **JUnit, Mockito** · Unit Tests
package com.currency.domain.usecases

import com.currency.domain.models.ExchangeRate
import com.currency.domain.repositories.ExchangeRateRepository
import io.reactivex.Single

class GetExchangeRateListUseCase(private val repository: ExchangeRateRepository) {

    private var shouldUpdateDb = false

    fun setShouldUpdateDb(update: Boolean) {
        shouldUpdateDb = update
    }

    fun shouldUpdateDb() = shouldUpdateDb

    fun load(): Single<List<ExchangeRate>> =
        repository.getAllExchangeRateList(shouldUpdateDb)

}
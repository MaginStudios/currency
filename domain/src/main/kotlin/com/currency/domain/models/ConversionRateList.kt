package com.currency.domain.models

data class ConversionRateList(
    val quotes: Map<String, Double> = emptyMap()
)
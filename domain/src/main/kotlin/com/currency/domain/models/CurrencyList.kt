package com.currency.domain.models

data class CurrencyList(
    val currencies: Map<String, String> = emptyMap()
)
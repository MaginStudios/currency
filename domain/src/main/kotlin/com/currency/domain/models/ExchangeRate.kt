package com.currency.domain.models

data class ExchangeRate(
    val code: String = "",
    val description: String = "",
    val value: Double = 0.0
)
package com.currency.domain.repositories.local

interface ExchangeRateLocalModelMapper<E, M> {
    fun fromEntity(from: E): M
    fun toEntity(from: M): E
}
package com.currency.domain.repositories.local

import com.currency.domain.models.ExchangeRate
import io.reactivex.Completable
import io.reactivex.Single

interface ExchangeRateLocalRepository {

    fun insertOrUpdate(exchangeRateList: List<ExchangeRate>): Completable

    fun getAllExchangeRateList(): Single<List<ExchangeRate>>
}
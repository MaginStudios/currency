package com.currency.domain.repositories.remote

import com.currency.domain.models.ConversionRateList
import com.currency.domain.models.CurrencyList
import io.reactivex.Single

interface ExchangeRateRemoteRepository {

    fun getAllCurrencyList(): Single<CurrencyList>

    fun getAllConversionRateList(): Single<ConversionRateList>
}
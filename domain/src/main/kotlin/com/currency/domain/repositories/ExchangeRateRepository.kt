package com.currency.domain.repositories

import com.currency.domain.models.ExchangeRate
import io.reactivex.Single

interface ExchangeRateRepository {

    fun getAllExchangeRateList(shouldUpdateDb: Boolean): Single<List<ExchangeRate>>
}
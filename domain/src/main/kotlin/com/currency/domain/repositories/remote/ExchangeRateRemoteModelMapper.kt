package com.currency.domain.repositories.remote

interface ExchangeRateRemoteModelMapper<E, M> {
    fun fromEntity(from: E): M
}
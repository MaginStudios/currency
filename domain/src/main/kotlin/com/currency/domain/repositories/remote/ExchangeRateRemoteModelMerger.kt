package com.currency.domain.repositories.remote

interface ExchangeRateRemoteModelMerger<A, B, M> {
    fun fromModels(modelA: A, modelB: B): M
}
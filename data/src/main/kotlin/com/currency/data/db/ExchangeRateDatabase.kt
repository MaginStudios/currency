package com.currency.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room.*
import androidx.room.RoomDatabase
import com.currency.data.entities.ExchangeRateLocalEntity
import com.currency.data.repositories.local.ExchangeRateLocalDaoImpl

fun createExchangeRateDao(context: Context): ExchangeRateLocalDaoImpl {
    return databaseBuilder(context, ExchangeRateDatabase::class.java, "exchangeratedb")
        .allowMainThreadQueries()
        .build().exchangeRateDao()
}

@Database(entities = arrayOf(ExchangeRateLocalEntity::class), version = 1, exportSchema = false)
internal abstract class ExchangeRateDatabase : RoomDatabase() {

    abstract fun exchangeRateDao(): ExchangeRateLocalDaoImpl
}
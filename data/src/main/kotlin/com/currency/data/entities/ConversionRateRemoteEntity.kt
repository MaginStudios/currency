package com.currency.data.entities

import androidx.room.Entity

@Entity
data class ConversionRateRemoteEntity(
    val quotes: Map<String, Double>
)
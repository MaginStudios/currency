package com.currency.data.entities

import androidx.room.Entity

@Entity
data class CurrencyRemoteEntity(
    val currencies: Map<String, String>
)
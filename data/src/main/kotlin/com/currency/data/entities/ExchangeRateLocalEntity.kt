package com.currency.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "exchange_rate")
data class ExchangeRateLocalEntity(
    @PrimaryKey(autoGenerate = false) val code: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "value") val value: Double)
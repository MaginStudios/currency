package com.currency.data.repositories

import com.currency.data.repositories.remote.ExchangeRateRemoteModelMergerImpl
import com.currency.domain.models.ExchangeRate
import com.currency.domain.repositories.local.ExchangeRateLocalRepository
import com.currency.domain.repositories.remote.ExchangeRateRemoteRepository
import com.currency.domain.repositories.ExchangeRateRepository
import io.reactivex.Single
import timber.log.Timber

class ExchangeRateRepositoryImpl(
    private val localRepository: ExchangeRateLocalRepository,
    private val remoteRepository: ExchangeRateRemoteRepository,
    private val merger: ExchangeRateRemoteModelMergerImpl
) : ExchangeRateRepository {

    override fun getAllExchangeRateList(shouldUpdateDb: Boolean): Single<List<ExchangeRate>> {
        if (shouldUpdateDb) {
            Timber.i(">> Remote fetch")
            return remoteRepository.getAllCurrencyList().flatMap { currencyList ->
                Timber.i(">> Remote fetch: getAllCurrencyList()")
                remoteRepository.getAllConversionRateList().flatMap {
                        conversionRateList ->
                    Timber.i(">> Remote fetch: getAllConversionRateList()")
                    val exchangeRateList = merger.fromModels(currencyList, conversionRateList)
                    localRepository.insertOrUpdate(exchangeRateList)
                        .andThen(Single.just(exchangeRateList))
                }
            }
        }
        Timber.i(">> Local fetch")
        return localRepository.getAllExchangeRateList().flatMap {
            Timber.i(">> Local fetch: getAllExchangeRateList()")
            if (it.isEmpty()) {
                Timber.i(">> Local fetch: isEmpty()")
                throw NoSuchElementException()
            }
            Single.just(it)
        }
    }

}
package com.currency.data.repositories.remote

import com.currency.domain.models.ConversionRateList
import com.currency.domain.models.CurrencyList
import com.currency.domain.repositories.remote.ExchangeRateRemoteRepository
import io.reactivex.Single

class ExchangeRateRemoteRepositoryImpl(
    private val exchangeRateService : ExchangeRateRemoteServiceImpl,
    private val currencyMapper: CurrencyRemoteModelMapperImpl,
    private val conversionRateMapper: ConversionRateRemoteModelMapperImpl
): ExchangeRateRemoteRepository {

    override fun getAllCurrencyList(): Single<CurrencyList> {
        return exchangeRateService.currencyList.map { currencyMapper.fromEntity(it) }
    }

    override fun getAllConversionRateList(): Single<ConversionRateList> {
        return exchangeRateService.conversionRateList.map { conversionRateMapper.fromEntity(it) }
    }

}
package com.currency.data.repositories.local

import com.currency.data.entities.ExchangeRateLocalEntity
import com.currency.domain.models.ExchangeRate
import com.currency.domain.repositories.local.ExchangeRateLocalModelMapper

class ExchangeRateLocalModelMapperImpl :
    ExchangeRateLocalModelMapper<ExchangeRateLocalEntity, ExchangeRate> {

    override fun fromEntity(from: ExchangeRateLocalEntity) =
        ExchangeRate(from.code, from.description, from.value)

    override fun toEntity(from: ExchangeRate) = ExchangeRateLocalEntity(
        from.code,
        from.description,
        from.value
    )
}
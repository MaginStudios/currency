package com.currency.data.repositories.remote

import com.currency.data.entities.CurrencyRemoteEntity
import com.currency.domain.models.CurrencyList
import com.currency.domain.repositories.remote.ExchangeRateRemoteModelMapper

class CurrencyRemoteModelMapperImpl :
    ExchangeRateRemoteModelMapper<CurrencyRemoteEntity, CurrencyList> {

    override fun fromEntity(from: CurrencyRemoteEntity): CurrencyList =
        CurrencyList(from.currencies)

}
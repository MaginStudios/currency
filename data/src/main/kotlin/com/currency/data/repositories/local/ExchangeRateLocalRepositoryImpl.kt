package com.currency.data.repositories.local

import com.currency.domain.models.ExchangeRate
import com.currency.domain.repositories.local.ExchangeRateLocalRepository
import io.reactivex.Completable
import io.reactivex.Single

class ExchangeRateLocalRepositoryImpl(
    private val exchangeRateDbLocalDao: ExchangeRateLocalDaoImpl,
    private val mapper: ExchangeRateLocalModelMapperImpl
) : ExchangeRateLocalRepository {

    override fun insertOrUpdate(exchangeRateList: List<ExchangeRate>) : Completable =
        Completable.fromAction{exchangeRateDbLocalDao.insertOrUpdate(exchangeRateList.map { mapper.toEntity(it) })}

    override fun getAllExchangeRateList(): Single<List<ExchangeRate>> {
        return exchangeRateDbLocalDao.getAllExchangeRateList().map { it.map(mapper::fromEntity) }
    }

}
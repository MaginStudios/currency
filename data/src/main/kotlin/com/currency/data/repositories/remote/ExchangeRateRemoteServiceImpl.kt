package com.currency.data.repositories.remote

import com.currency.data.entities.ConversionRateRemoteEntity
import com.currency.data.entities.CurrencyRemoteEntity
import io.reactivex.Single
import retrofit2.http.GET


interface ExchangeRateRemoteServiceImpl  {

    @get:GET("live")
    val conversionRateList: Single<ConversionRateRemoteEntity>

    @get:GET("list")
    val currencyList: Single<CurrencyRemoteEntity>

}
package com.currency.data.repositories.remote

import com.currency.data.entities.ConversionRateRemoteEntity
import com.currency.domain.models.ConversionRateList
import com.currency.domain.repositories.remote.ExchangeRateRemoteModelMapper

class ConversionRateRemoteModelMapperImpl :
    ExchangeRateRemoteModelMapper<ConversionRateRemoteEntity, ConversionRateList> {

    override fun fromEntity(from: ConversionRateRemoteEntity): ConversionRateList =
        ConversionRateList(from.quotes)

}
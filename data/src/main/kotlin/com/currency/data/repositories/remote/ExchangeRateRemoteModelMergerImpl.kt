package com.currency.data.repositories.remote

import com.currency.domain.models.ConversionRateList
import com.currency.domain.models.CurrencyList
import com.currency.domain.models.ExchangeRate
import com.currency.domain.repositories.remote.ExchangeRateRemoteModelMerger

class ExchangeRateRemoteModelMergerImpl :
    ExchangeRateRemoteModelMerger<CurrencyList, ConversionRateList, List<ExchangeRate>> {

    override fun fromModels(
        modelA: CurrencyList,
        modelB: ConversionRateList
    ): List<ExchangeRate> {
        return modelA.currencies.map { currency ->
            // TODO: Define correct values for unavailable currencies
            val value = modelB.quotes["USD" + currency.key] ?: 1.0
            ExchangeRate(currency.key, currency.value, value)
        }
    }
}
package com.currency.data.repositories.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.currency.data.entities.ExchangeRateLocalEntity
import com.currency.domain.repositories.local.ExchangeRateLocalDao
import io.reactivex.Single

@Dao
interface ExchangeRateLocalDaoImpl :
    ExchangeRateLocalDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(exchangeRateList: List<ExchangeRateLocalEntity>)

    @Query("SELECT * FROM exchange_rate ORDER BY code ASC")
    fun getAllExchangeRateList(): Single<List<ExchangeRateLocalEntity>>
}
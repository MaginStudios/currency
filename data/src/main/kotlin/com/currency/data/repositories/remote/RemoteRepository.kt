package com.currency.data.repositories.remote

import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient

class RemoteRepository {
    fun <T> create(clazz: Class<T>, baseUrl: String): T {
        return retrofit(baseUrl).create(clazz)
    }

    private fun retrofit(baseUrl: String): Retrofit {
        val okHttpClient = OkHttpClient().newBuilder().addInterceptor(Interceptor {
            var request = it.request()
            val url = request.url().newBuilder()
                .addQueryParameter("access_key", "3afaee0f0e1af4d0867c72fc55beb4b3")
                .build()
            request = request.newBuilder().url(url).build()
            it.proceed(request)
        }).build()

        val gsonBuilder = GsonBuilder()

        val customGson = gsonBuilder.create()

        return Retrofit.Builder()
            .baseUrl(baseUrl).client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(customGson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }
}
package com.currency.data.service

import com.currency.data.repositories.remote.ExchangeRateRemoteServiceImpl
import com.currency.data.repositories.remote.RemoteRepository

fun createExchangeRateService() : ExchangeRateRemoteServiceImpl {
    return RemoteRepository().create(
        ExchangeRateRemoteServiceImpl::class.java,
        "http://api.currencylayer.com/"
    )
}
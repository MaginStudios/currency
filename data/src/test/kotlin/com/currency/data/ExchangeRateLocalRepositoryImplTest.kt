package com.currency.data

import com.currency.data.entities.ExchangeRateLocalEntity
import com.currency.data.repositories.local.ExchangeRateLocalDaoImpl
import com.currency.data.repositories.local.ExchangeRateLocalModelMapperImpl
import com.currency.data.repositories.local.ExchangeRateLocalRepositoryImpl
import com.currency.domain.models.ExchangeRate
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.hamcrest.MatcherAssert
import org.junit.Test
import org.hamcrest.Matchers.`is` as is_

class ExchangeRateLocalRepositoryImplTest {

    private val exchangeRateDao: ExchangeRateLocalDaoImpl = mock()

    private val exchangeRateMapper: ExchangeRateLocalModelMapperImpl = mock()

    private val testSubject: ExchangeRateLocalRepositoryImpl =
        ExchangeRateLocalRepositoryImpl(exchangeRateDao, exchangeRateMapper)

    @Test
    fun insertOrUpdate_emitsOnSuccess() {
        val exchangeRate = exchangeRate()
        val exchangeRateList = listOf(exchangeRate)
        val exchangeRateLocalEntities = given_entitiesFromExchangeRateList(exchangeRateList)

        val testObserver = testSubject.insertOrUpdate(exchangeRateList).test()

        testObserver.assertComplete()
        argumentCaptor<List<ExchangeRateLocalEntity>>().apply {
            verify(exchangeRateDao).insertOrUpdate(capture())
            MatcherAssert.assertThat(firstValue.first().code, is_(exchangeRateLocalEntities.first().code))
            MatcherAssert.assertThat(firstValue.first().description, is_(exchangeRateLocalEntities.first().description))
            MatcherAssert.assertThat(firstValue.first().value, is_(exchangeRateLocalEntities.first().value))
        }
    }

    @Test
    fun getAllExchangeRateList_emitsValues() {
        val entity = exchangeRateLocalEntity()
        val exchangeRateEntities = listOf(entity)
        val exchangeRate = given_exchangeRateFromEntity(entity)
        whenever(exchangeRateDao.getAllExchangeRateList()).thenReturn(Single.just(exchangeRateEntities))

        val testObserver = testSubject.getAllExchangeRateList().test()

        verify(exchangeRateMapper).fromEntity(entity)
        testObserver.assertResult(listOf(exchangeRate))
        testObserver.assertComplete()
    }

    private fun exchangeRate() = ExchangeRate("USD", "test exchangeRate", 1.0)

    private fun exchangeRateLocalEntity() =
        ExchangeRateLocalEntity("USD", "test entity", 1.0)

    private fun given_entitiesFromExchangeRateList(exchangeRateList: List<ExchangeRate>): List<ExchangeRateLocalEntity>{
        val exchangeRateEntity: ExchangeRateLocalEntity = mock()
        whenever(exchangeRateMapper.toEntity(exchangeRateList.first())).thenReturn(exchangeRateEntity)
        whenever(exchangeRateEntity.code).thenReturn(exchangeRateList.first().code)
        whenever(exchangeRateEntity.description).thenReturn(exchangeRateList.first().description)
        whenever(exchangeRateEntity.value).thenReturn(exchangeRateList.first().value)
        return listOf(exchangeRateEntity)
    }

    private fun given_exchangeRateFromEntity(exchangeRateEntity: ExchangeRateLocalEntity): ExchangeRate {
        val exchangeRate: ExchangeRate = mock()
        whenever(exchangeRateMapper.fromEntity(exchangeRateEntity)).thenReturn(exchangeRate)
        whenever(exchangeRate.code).thenReturn(exchangeRateEntity.code)
        whenever(exchangeRate.description).thenReturn(exchangeRateEntity.description)
        whenever(exchangeRate.value).thenReturn(exchangeRateEntity.value)
        return exchangeRate
    }
}
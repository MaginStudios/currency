package com.currency.data

import com.currency.data.entities.ConversionRateRemoteEntity
import com.currency.data.entities.CurrencyRemoteEntity
import com.currency.data.repositories.remote.ConversionRateRemoteModelMapperImpl
import com.currency.data.repositories.remote.CurrencyRemoteModelMapperImpl
import com.currency.data.repositories.remote.ExchangeRateRemoteRepositoryImpl
import com.currency.data.repositories.remote.ExchangeRateRemoteServiceImpl
import com.currency.domain.models.ConversionRateList
import com.currency.domain.models.CurrencyList
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Test

class ExchangeRateRemoteRepositoryImplTest {

    private val exchangeRateService: ExchangeRateRemoteServiceImpl = mock()
    private val currencyMapper: CurrencyRemoteModelMapperImpl = mock()
    private val conversionRateMapper: ConversionRateRemoteModelMapperImpl = mock()

    private val testSubject: ExchangeRateRemoteRepositoryImpl =
        ExchangeRateRemoteRepositoryImpl(exchangeRateService, currencyMapper, conversionRateMapper)

    @Test
    fun getAllCurrencyList_emitsValues() {
        val entity = currencyListRemoteEntity()
        val currencyList = given_currencyListFromEntity(entity)
        whenever(exchangeRateService.currencyList).thenReturn(Single.just(entity))

        val testObserver = testSubject.getAllCurrencyList().test()

        testObserver.assertResult(currencyList)
        testObserver.assertComplete()
    }

    private fun currencyListRemoteEntity() = CurrencyRemoteEntity(mapOf("USD" to "United States Dollar"))

    private fun given_currencyListFromEntity(currencyEntity: CurrencyRemoteEntity): CurrencyList {
        val currencyList: CurrencyList = mock()
        whenever(currencyMapper.fromEntity(currencyEntity)).thenReturn(currencyList)
        whenever(currencyList.currencies).thenReturn(currencyEntity.currencies)
        return currencyList
    }

    @Test
    fun getAllConversionRateList_emitsValues() {
        val entity = conversionRateListRemoteEntity()
        val conversionRateList = given_conversionRateListFromEntity(entity)
        whenever(exchangeRateService.conversionRateList).thenReturn(Single.just(entity))

        val testObserver = testSubject.getAllConversionRateList().test()

        testObserver.assertResult(conversionRateList)
        testObserver.assertComplete()
    }

    private fun conversionRateListRemoteEntity() = ConversionRateRemoteEntity(mapOf("USD" to 1.0))

    private fun given_conversionRateListFromEntity(conversionRateEntity: ConversionRateRemoteEntity): ConversionRateList {
        val conversionRateList: ConversionRateList = mock()
        whenever(conversionRateMapper.fromEntity(conversionRateEntity)).thenReturn(conversionRateList)
        whenever(conversionRateList.quotes).thenReturn(conversionRateEntity.quotes)
        return conversionRateList
    }
}